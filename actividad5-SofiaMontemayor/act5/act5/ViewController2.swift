//
//  ViewController2.swift
//  act5
//
//  Created by Tecmilenio on 14/02/19.
//  Copyright © 2019 jfrn. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController2: UIViewController{
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var song: UILabel!
    @IBOutlet weak var artist: UILabel!
    @IBOutlet weak var musicSlider: UISlider!
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var previousbtn: UIButton!
    @IBOutlet weak var playbtn: UIButton!
    @IBOutlet weak var nextbtn: UIButton!
    @IBOutlet weak var backbtn: UIButton!
    var temporal = 0;
    var vessel = 0;
    var player = AVAudioPlayer()
    let songs = ["Meant to be","She used to be Mine","Angel Eyes","Bailar Contigo ","Between the Line"];
    let artists = ["Bebe Rexha","Sara Bareilles","Mamma Mia", "Monsieur Perine","Sara Bareilles"];
    var pathSong = ["MeantToBe","SheUsedToBeMine","AngelEyes","BailarContigo","BetweenTheLines"];
    
    let img = [#imageLiteral(resourceName: "meant-to-be"),#imageLiteral(resourceName: "waitress"),#imageLiteral(resourceName: "mamma-mia"),#imageLiteral(resourceName: "bailar-contigo"),#imageLiteral(resourceName: "between")];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photo.image = img[vessel];
        song.text = songs[vessel];
        artist.text = artists[vessel];
        
        do {
            try player = AVAudioPlayer(contentsOf : URL(fileURLWithPath: Bundle.main.path(forResource: pathSong[vessel], ofType: "mp3")!))
        } catch {
            
        }
        //Que se mueva solo el slider
        musicSlider.maximumValue = Float(player.duration)
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
        
        //Previous botton
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tap))  //Tap function will call when user tap on button
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(long))  //Long function will call when user long press on button.
        tapGesture.numberOfTapsRequired = 1
        previousbtn.addGestureRecognizer(tapGesture)
        previousbtn.addGestureRecognizer(longGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func updateSlider(){
        musicSlider.value = Float(player.currentTime)
    }
    
    @IBAction func sliderMove(_ sender: Any) {
        player.stop()
        player.currentTime = TimeInterval(musicSlider.value)
        player.play()
    }
    
    @objc func tap() {
        if player.isPlaying{
            player.pause()
            player.currentTime = 0
            player.play()
        }else{
            player.play()
        }
        
    }
    
    @objc func long() {
        photo.image = img[vessel - 1];
        song.text = songs[vessel - 1];
        artist.text = artists[vessel - 1];
        
    }

    /*---------------------------------CambioBtnPlayPause--------------------------------------*/
    var vali = true
    @IBAction func changePlay(_ sender: Any) {
        if vali == true{
            player.play()
            vali = false
            playbtn.setTitle("Pause", for: .normal)
        }else{
            player.pause()
            vali = true
            playbtn.setTitle("Play", for: .normal)
        }
    }
    
    
    
     /*---------------------------------sliderVolume--------------------------------------*/
    
    @IBAction func volSlider(_ sender: Any) {
        player.volume = volumeSlider.value
    }
    /*---------------------------------Back btn--------------------------------------*/
    
    @IBAction func stopBack(_ sender: Any) {
        player.stop()
    }
    
     /*---------------------------------previousbtn--------------------------------------*/
    
    
    @IBAction func nextSong(_ sender: Any) {
        photo.image = img[vessel + 1];
        song.text = songs[vessel + 1];
        artist.text = artists[vessel + 1];
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
 

}
